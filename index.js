const nodemailer = require('nodemailer');
require('dotenv').config();

const config = {
    smtp: {
        outlook: {
            host: 'smtp-mail.outlook.com',
            port: 587,
            secure: false,  // true para a porta 465 e false para as outras
            auth: {
                user: process.env.OUTLOOK_USER,
                pass: process.env.OUTLOOK_PASS
            }
        },        
        gmail: {
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,  // true para a porta 465 e false para as outras
            auth: {
                user: process.env.GMAIL_USER,
                pass: process.env.GMAIL_PASS    // Not your password mail, but the app password generated in Google Account > Security > "How do sign in to Google" : Two-step verification > App passwords
            }
        }
    },
    send: {
        outlook: {
            from: `<${process.env.FROM_MAIL_MY}>`,
            to: process.env.TO_MAIL_OTHER,
            subject: 'Teste Envio de Email via NodeJS',
            html: '<h1>Olá Dev!</h1><p>Esse email foi enviado usando o Nodemailer</p>',
            text: 'Olá Dev! Esse email foi enviado usando o Nodemailer'
        },
        gmail: {
            from: `<${process.env.FROM_MAIL_MY}>`,
            to: process.env.TO_MAIL_MY,
            subject: 'Teste Envio de Email via NodeJS',
            html: '<h1>Olá Dev!</h1><p>Esse email foi enviado usando o Nodemailer</p>',
            text: 'Olá Dev! Esse email foi enviado usando o Nodemailer'
        }
    }
};

const transport = nodemailer.createTransport(config.smtp.gmail);

transport.sendMail(config.send.gmail)
    .then(response => console.log('Email enviado com sucesso!'))
    .catch(err => console.log(`Erro ao enviar o email: ${err}`));