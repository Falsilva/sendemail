# NodeJS - [Nodemailer](https://www.npmjs.com/package/nodemailer)
> node v21.5.0

> nodemailer v6.9.8

> dotenv v16.3.1

## App que faz o envio de email via SMTP Outlook ou Gmail. 
Construído seguindo o tutorial do vídeo do YouTube:

Canal [Manual do Dev](https://www.youtube.com/@ManualdoDev) Vídeo [Como enviar email com Node.js](https://www.youtube.com/watch?v=q2sPzKgBMaA)